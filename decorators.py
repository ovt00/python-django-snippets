from functools import wraps
from time import perf_counter

from django.db import reset_queries, connection
from django.urls import reverse
from django.utils.html import format_html


def delete_upload_file(cls):
    """ Delete upload files when deleting and saving instance """

    import os

    def delete_empty_dir(path: str) -> None:
        try:
            os.rmdir(path)
        except (Exception, ):
            pass

    def save(self, *args, **kwargs) -> None:
        try:
            this_record = cls.objects.get(id=self.id)
        except (Exception, ):
            pass
        else:
            for field in this_record._meta.fields:
                if field.get_internal_type() == 'FileField' or field.get_internal_type() == 'ImageField':
                    field_file = getattr(this_record, field.name)
                    if field_file.name and field_file != getattr(self, field.name):
                        path_dir = os.path.split(field_file.path)[0]
                        field_file.delete(save=False)
                        delete_empty_dir(path_dir)
        cls.save_original(self, *args, **kwargs)

    def delete(self, *args, **kwargs) -> None:
        fields = []
        for field in self._meta.fields:
            if field.get_internal_type() == 'FileField' or field.get_internal_type() == 'ImageField':
                field_file = getattr(self, field.name)
                if field_file.name:
                    fields.append(dict(
                        storage=field_file.storage,
                        path=field_file.path,
                    ))

        cls.delete_original(self, *args, **kwargs)

        for field in fields:
            path_dir = os.path.split(field['path'])[0]
            field['storage'].delete(field['path'])
            delete_empty_dir(path_dir)

    cls.save_original = cls.save
    cls.delete_original = cls.delete
    cls.save = save
    cls.delete = delete

    return cls


def profiling_func(func):
    """ profiling method or function """

    @wraps(func)
    def inner_func(*args, **kwargs):
        reset_queries()

        start_queries = len(connection.queries)

        start = perf_counter()
        result = func(*args, **kwargs)
        end = perf_counter()

        end_queries = len(connection.queries)

        print(f"Function : {func.__name__}")
        print(f"Number of Queries : {end_queries - start_queries}")
        print(f"Finished in : {(end - start):.2f}s")
        return result

    return inner_func


def modify_fields(**kwargs):
    """
    Модификация параметров полей в моделе, например, для переопределения verbose_name в унаследованных полях.
    :param kwargs: аттрибут = {название параметра: значение}
    :return: возврашщает класс с модифицированными параметрами поля
    """

    def wrap(cls):
        for field, prop_dict in kwargs.items():
            for prop, val in prop_dict.items():
                setattr(cls._meta.get_field(field), prop, val)
        return cls

    return wrap


def admin_link(attr, short_description, empty_description="-"):
    """Decorator used for rendering a link to a related model in
    the admin detail page.
    attr (str):
        Name of the related field.
    short_description (str):
        Name if the field.
    empty_description (str):
        Value to display if the related field is None.
    The wrapped method receives the related object and should
    return the link text.
    Usage:
        @admin_link('credit_card', _('Credit Card'))
        def credit_card_link(self, credit_card):
            return credit_card.name
    """

    def admin_change_url(obj):
        app_label = obj._meta.app_label
        model_name = obj._meta.model.__name__.lower()
        return reverse('admin:{}_{}_change'.format(
            app_label, model_name
        ), args=(obj.pk,))
    
    def wrap(func):
        def field_func(self, obj):
            related_obj = getattr(obj, attr)
            if related_obj is None:
                return empty_description
            url = admin_change_url(related_obj)
            return format_html(
                '<a href="{}">{}</a>',
                url,
                func(self, related_obj)
            )
        field_func.short_description = short_description
        field_func.allow_tags = True
        return field_func
    return wrap


def admin_changelist_link(
        attr,
        short_description,
        empty_description="-",
        query_string=None
):
    """Decorator used for rendering a link to the list display of
    a related model in the admin detail page.
    attr (str):
        Name of the related field.
    short_description (str):
        Field display name.
    empty_description (str):
        Value to display if the related field is None.
    query_string (function):
        Optional callback for adding a query string to the link.
        Receives the object and should return a query string.
    The wrapped method receives the related object and
    should return the link text.
    Usage:
        @admin_changelist_link('credit_card', _('Credit Card'))
        def credit_card_link(self, credit_card):
            return credit_card.name
    """

    def admin_changelist_url(model):
        app_label = model._meta.app_label
        model_name = model.__name__.lower()
        return reverse('admin:{}_{}_changelist'.format(
            app_label,
            model_name)
        )    
        
    def wrap(func):
        def field_func(self, obj):
            related_obj = getattr(obj, attr)
            if related_obj is None:
                return empty_description
            url = admin_changelist_url(related_obj.model)
            if query_string:
                url += '?' + query_string(obj)
            return format_html(
                '<a href="{}">{}</a>',
                url,
                func(self, related_obj)
            )
        field_func.short_description = short_description
        field_func.allow_tags = True
        return field_func
    return wrap

