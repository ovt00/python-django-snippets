from django.contrib.admin.widgets import AdminFileWidget


class ImagePreviewWidget(AdminFileWidget):
    """ Виджет отображения картинки в форме изменения в админчасти """

    template_name = 'widgets/clearable_file_input.html'

    class Media:
        js = (
            'js/clearable_file_input.js',
        )

    def get_context(self, name, value, attrs):
        """ 
        Переопределите метод в админке django так:
        def formfield_for_dbfield(self, db_field, request, **kwargs):
            if db_field.name in self.image_fields:
                # определяет высоту виджета
                kwargs.pop("request", None)
                kwargs['widget'] = ImagePreviewWidget(attrs=dict(height_img=60))
                return db_field.formfield(**kwargs)
        
            return super().formfield_for_dbfield(db_field, request, **kwargs)
            
        и задайте в аттрибуте класса поля, на которых желаете увидеть превью
        image_fields = ['img', ]
        """
        context = super().get_context(name, value, attrs)
        context['widget'].update({
            'height_img': self.attrs.get('height_img', 100)
        })
        return context
