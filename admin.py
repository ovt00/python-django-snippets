from django.contrib import admin

admin.site.favicon = 'path_to_favicon'
admin.site.site_title = 'Change title “Django site admin”'
admin.site.site_header = 'Change header “Django administration” in the Login, Listview and Changeview Page'
admin.site.index_title = 'Change title “Site administration” in the Listview'