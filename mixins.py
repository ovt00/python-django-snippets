from django.contrib.admin.views.main import ChangeList
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.db import models


class HasChangedFieldsMixin(object):
    def __init__(self, *args, **kwargs):
        """ 
        отслеживаем поля, перечисленные в моделе в FIELDS_CHANGE или все поля.
        Для полей ForeignKey добавлею _id, чтобы не делать лишних запросов к БД.
        """

        super().__init__(*args, **kwargs)

        self.__important_fields = getattr(self, 'FIELDS_CHANGE', tuple(map(
            lambda field: field.name if not isinstance(field, models.ForeignKey) else f'{field.name}_id',
            self._meta.fields)))

        self.set_original_fields()

    def has_changed(self):
        for field in self.__important_fields:
            orig = '__original_%s' % field
            if getattr(self, orig) != getattr(self, field, None):
                return True
        return False

    def get_changed(self):
        fields = []
        for field in self.__important_fields:
            orig = '__original_%s' % field
            if getattr(self, orig) != getattr(self, field, None):
                fields.append(field)
        return fields

    def set_original_fields(self):
        """ записываем значение, отслеживаемых полей """

        for field in self.__important_fields:
            setattr(self, f'__original_{field}', getattr(self, field, None))


class PaginationInlineMixin(object):
    """
    Михин добавления пагинации в inline admin формы
    """
    template = 'admin/edit_inline/tabular_paginated.html'
    list_per_page = 10

    def get_formset(self, request, obj=None, **kwargs):
        formset_class = super().get_formset(request, obj, **kwargs)

        class InlineChangeList(object):
            can_show_all = True
            multi_page = True
            get_query_string = ChangeList.__dict__['get_query_string']

            def __init__(self, request, page_num, paginator):
                self.show_all = 'all' in request.GET
                self.page_num = page_num
                self.paginator = paginator
                self.result_count = paginator.count
                self.params = dict(request.GET.items())
                self.full_result_count = paginator.count

        class PaginationFormSet(formset_class):
            def __init__(self, *args, **kwargs):
                super(PaginationFormSet, self).__init__(*args, **kwargs)

                qs = self.queryset
                paginator = Paginator(qs, self.list_per_page)
                try:
                    page_num = int(request.GET.get(qs.model._meta.db_table + '__p', '0'))
                except ValueError:
                    page_num = 0

                try:
                    page = paginator.page(page_num + 1)
                except (EmptyPage, InvalidPage):
                    page = paginator.page(paginator.num_pages)

                self.cl = InlineChangeList(request, page_num, paginator)
                self.paginator = paginator

                if self.cl.show_all:
                    self._queryset = qs
                else:
                    self._queryset = page.object_list

        PaginationFormSet.list_per_page = self.list_per_page
        return PaginationFormSet
