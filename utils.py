from django.db import transaction, connection, OperationalError
from django.utils.functional import cached_property


class Singleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance
        
        
class TimeLimitedSQLRequest(object):
    """
    Class that enforces a timeout on the some operation.
    If the operations times out, a fake value is
    returned instead.
    """

    @cached_property
    def some(self):
        # We set the timeout in a db transaction to prevent it from
        # affecting other transactions.
        with transaction.atomic(), connection.cursor() as cursor:
            cursor.execute('SET LOCAL statement_timeout TO 200;')
            try:
                return super().some()
            except OperationalError:
                # fake value or do other action
                return 9999999999
