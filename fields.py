from django.conf.locale import et
from django.db import models
from django import forms
from django.core.exceptions import ValidationError
import xml.etree.cElementTree as cElementTree
from django.core.validators import (
    get_available_image_extensions,
    FileExtensionValidator,
)


class ImageWithSVGField(models.ImageField):
    """ Добавляем в ImageField поддержку svg формата """    

    class ImageWithSVGFieldForm(forms.ImageField):
        
        @staticmethod
        def validate_image_and_svg_file_extension(value: str):
            """ Валидатор расширений файлов. Добавляем svg к списку разрешеных расширений """
            
            allowed_extensions = get_available_image_extensions()
            allowed_extensions.append("svg")
            return FileExtensionValidator(allowed_extensions=allowed_extensions)(value)

        @staticmethod
        def validate_svg(f):
            """ Find "start" word in file and get "tag" from there """
            
            f.seek(0)
            tag = None
            try:
                for event, el in et.iterparse(f, ('start',)):
                    tag = el.tag
                    break
            except cElementTree.ParseError:
                pass
        
            # Check that this "tag" is correct
            if tag != '{http://www.w3.org/2000/svg}svg':
                raise ValidationError('Uploaded file is not an image or SVG file.')
        
            # Do not forget to "reset" file
            f.seek(0)
        
            return f        

        default_validators = [validate_image_and_svg_file_extension]        
   
        def to_python(self, data):
            try:
                f = super().to_python(data)
            except ValidationError:
                return self.validate_svg(data)
    
            return f    
    
    def formfield(self, **kwargs):
        defaults = {'form_class': self.ImageWithSVGFieldForm}
        defaults.update(kwargs)
        return super().formfield(**defaults)
