$(document).ready(function () {

  function get_image_thumbnail_html(src, height_img) {
    return '<a href="' + src + '" target="_blank"><img src="' + src + '" alt="' + src + '" height=' + height_img + '></a>';
  }

  function get_alert_html(message) {
    let html = [];
    html.push('<div class="alert alert-danger alert-dismissible">');
    html.push('<button type="button" class="close" data-dismiss="alert">');
    html.push('<span>&times;</span>');
    html.push('</button>' + message);
    html.push('</div>');
    return html.join('');
  }

  $('.btn-browse').on('change', function() {
    $(this).blur();
    let controls = $(this).closest('.file-input').find('.controls');
    let height_img = controls.data("height_img");
    let file_input = $(this).closest('.file-input').find('input[type=file]');
    let remove_button = $(this).closest('.file-input').find('.btn-remove');
    let browse_button = $(this).closest('.file-input').find('.btn-browse');
    let clear_input = $(this).closest('.file-input').find('input[type=checkbox]');
    controls.find('img').remove();

    // Check if file was uploaded.
    if (!(file_input[0].files && file_input[0].files[0])) {
      return;
    }

    $(this).prop('disabled', true);
    let file = file_input[0].files[0];
    let fileReader = new FileReader();
    fileReader.onload = function(e) {
      controls.prepend(get_image_thumbnail_html(e.target.result, height_img));
      remove_button.show();
    };

    fileReader.onerror = function() {
      controls.prepend(get_alert_html('Error loading image file.'));
      file_input.val('');
    };

    fileReader.readAsDataURL(file);
    clear_input.prop("checked", false);
    $(this).prop('disabled', false);
  });

  $('.btn-remove').on('click', function() {
    $(this).blur();
    let controls = $(this).closest('.file-input').find('.controls');
    let clear_input = $(this).closest('.file-input').find('input[type=checkbox]');
    controls.find('img').remove();
    $(this).hide();
    clear_input.prop("checked", true);
  });

});
